/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author PIYUSH
 */
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
public class HandGesture
{
    
    
    private int pointX;
    private int pointY;
    private static Robot robot;
    private boolean  isPresent=false;
    private boolean isGesture = false; 
    private JFrame frame; 
    private JLabel imageLabel;   

  
     public HandGesture() {
        initGUI();
        customInit();   
    }

      public static void main(String[] args) { 
                  new HandGesture();   
    }
      
           private void customInit() {
    
    	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  
        new Thread() {
            @Override
            public void run() {
                video();
            }
        }.start();

            try {
            robot = new Robot();
        } catch(AWTException awte) {
            System.out.println("AWT Exception caught : " + awte.getMessage());
        } catch(Exception e) {
            System.out.println("Exception caught : " + e.getMessage());
        }
    }



   private void initGUI() {
      frame = new JFrame("Camera Input Example");    
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
      frame.setSize(640,480);    
      imageLabel = new JLabel();   
      frame.add(imageLabel);    
      frame.setLocationRelativeTo(null); 
     frame.setVisible(true);         
}
  private void video() { 
    		
        VideoCapture capture = new VideoCapture(0); 
        
    
        Mat webcamMatImage = new Mat();
        
       
        capture.set(Videoio.CAP_PROP_FRAME_WIDTH,1766);
		capture.set(Videoio.CAP_PROP_FRAME_HEIGHT,868);


        
		if( capture.isOpened()){  
			while (true){  
				capture.read(webcamMatImage);  
				if( !webcamMatImage.empty() ){  
                    Mat inRange = new Mat();
                    Mat temp = new Mat();
                    
                    Imgproc.threshold(webcamMatImage,inRange, 150, 255, Imgproc.THRESH_BINARY);
                   
                    Core.inRange(inRange, new Scalar(0,0,250), new Scalar(0,0,255), temp);
         
                    if(getXCoordinate(temp) != -1) {
                    	
                        if(!isPresent || isGesture) {
                        	
                            this.pointX = getXCoordinate(temp);
                            this.pointY = getYCoordinate(temp);

                            this.isPresent = true;
                        }
                        else {
                          
                            int x = getXCoordinate(temp) - this.pointX;
                            int y = getYCoordinate(temp) - this.pointY;
                         
                            if((x > y && ( x >=0 || y >= 0)) || (x < y && (x<0 || y < 0))) {
                           
                               
                                 // robot.mouseMove(x, y);
                               
                                if(x >= 200) {
                                    robot.keyPress(KeyEvent.VK_RIGHT);
                                  
                                    isGesture = true;
                                }
                                else if( x <= -200) {
                                    robot.keyPress(KeyEvent.VK_LEFT);
                                     
                                    isGesture = true;
                                }
                                else if(x>=50){
                                    robot.keyPress(KeyEvent.VK_F5);
                                  
                                }     
                                else if(x <= -50){
                                    robot.keyPress(KeyEvent.VK_ESCAPE);
                                      
                                }
                            }
                        }
                    }
                    else {
                        this.isPresent = false;
                        this.isGesture = false;
                    }
                    this.imageLabel.setIcon(new ImageIcon(ImageProcessor.toBufferedImage(temp)));
				}  
				else{  
					System.out.println(" -- Frame not captured -- Break!"); 
					break;  
				}
			}  
		}
		else{
			System.out.println("Couldn't open capture.");
		}
    }
 
  
            private int  getXCoordinate(Mat src){
              byte[] buffer = new byte[((int) (src.total()*src.elemSize()))];
              src.get(0, 0,buffer);
              for(int i =0;i<src.height();i++)
              {
                    for(int j=0;j<src.width();j++) {
                        if(buffer[(src.width()*i)+j] == -1)
                            return j;
                    }
              }
                return -1;
          }
           private int getYCoordinate(Mat src) {
        byte[] buffer =  new byte[(int)(src.total() * src.elemSize())];
        src.get(0, 0, buffer);
        for(int i=0;i<src.height();i++) {
            for(int j=0;j<src.width();j++) {
                if(buffer[(src.width()*i) + j] == -1) {
                    return i;
                }
            }
        }
        return -1;
    }

 }
