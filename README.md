# hand-gesture-recognizier

Using a remote with visual aids makes presenting the content more easier. But as these remotes are expensive not everyone can afford it. Here comes HGR (Hand Gesture Recognizer) that lets you control you presentation using your hand. All you need to do is launch HGR, face your laptop towards the projector screen. That's it! Now you can control your presentation using your hand.
How it works
This application is developed in Java with OpenCV3.
Algorithm


Capture the video feed from the laptop's webcam.

Isolate the image by thresholding and picking the color range of hand.

Extract the largest contour from the image.

Recognize the direction of the hand and perform the specified operation